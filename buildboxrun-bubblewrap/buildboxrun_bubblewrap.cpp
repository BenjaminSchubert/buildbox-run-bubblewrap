/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxrun_bubblewrap.h>

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_mergeutil.h>
#include <buildboxcommon_platformutils.h>
#include <buildboxcommon_runner.h>
#include <buildboxcommon_systemutils.h>

#include <stdlib.h>
#include <sys/utsname.h>
#include <unistd.h>

namespace buildboxcommon {
namespace buildboxrun {
namespace bubblewrap {

BubbleWrapRunner::BubbleWrapRunner() : Runner()
{
    const std::string bubblewrapBin = "bwrap";

    /* Find the path to the bubblewrap binary. */
    d_bubblewrapBinPath = SystemUtils::getPathToCommand(bubblewrapBin);
    if (d_bubblewrapBinPath.empty()) {
        std::cerr << "Could not find bubblewrap command \"" << bubblewrapBin
                  << "\"\n";
        exit(1);
    }

    d_linux32Path = SystemUtils::getPathToCommand("linux32");

    d_userNamespaceAvailable = userNamespaceAvailable();
}

bool BubbleWrapRunner::userNamespaceAvailable()
{
    const auto maxUserNamespacesPath = "/proc/sys/user/max_user_namespaces";

    if (!FileUtils::isRegularFile("/proc/self/ns/user") ||
        !FileUtils::isRegularFile(maxUserNamespacesPath)) {
        /* Kernel too old or user namespace support disabled in config */
        return false;
    }

    /* Check whether user namespaces are disabled by the distro or admin */
    const auto maxUserNamespaces =
        FileUtils::getFileContents(maxUserNamespacesPath);
    return maxUserNamespaces != "0\n";
}

const std::vector<std::string>
BubbleWrapRunner::generateCommandLine(const Command &command,
                                      const std::string &root_path)
{
    std::vector<std::string> bwrap_argv;

    std::string uid("0");
    std::string gid("0");
    bool network = false;
    bool linux32 = false;

    /* Process platform properties */
    for (const auto &property : command.platform().properties()) {
        if (property.name() == "OSFamily") {
            if (property.value() != PlatformUtils::getHostOSFamily()) {
                std::cerr << "Unsupported OSFamily \"" << property.value()
                          << "\"\n";
                exit(1);
            }
        }
        else if (property.name() == "ISA") {
            auto host = PlatformUtils::getHostISA();

            if (property.value() == host) {
                linux32 = false;
            }
            else if (host == "x86-64" && property.value() == "x86-32") {
                linux32 = true;
            }
            else if (host == "aarch64" && property.value() == "aarch32") {
                linux32 = true;
            }
            else {
                std::cerr << "Unsupported ISA \"" << property.value()
                          << "\"\n";
                exit(1);
            }
        }
        else if (property.name() == "unixUID") {
            if (!d_userNamespaceAvailable) {
                std::cerr << "User namespaces are not available. "
                             "Cannot support unixUID.\n";
                exit(1);
            }
            uid = property.value();
        }
        else if (property.name() == "unixGID") {
            if (!d_userNamespaceAvailable) {
                std::cerr << "User namespaces are not available. "
                             "Cannot support unixGID.\n";
                exit(1);
            }
            gid = property.value();
        }
        else if (property.name() == "network") {
            network = property.value() == "on";
        }
    }

    if (linux32) {
        if (d_linux32Path.empty()) {
            std::cerr << "Could not find linux32 command\n";
            exit(1);
        }

        bwrap_argv.push_back(d_linux32Path);
    }

    bwrap_argv.push_back(d_bubblewrapBinPath);

    /* Create a new pid namespace, this also ensures that any subprocesses
     * are cleaned up when the bwrap process exits.
     */
    bwrap_argv.push_back("--unshare-pid");

    /* Ensure subprocesses are cleaned up when the bwrap parent dies. */
    bwrap_argv.push_back("--die-with-parent");

    /* Mount sandbox rootfs */
    bwrap_argv.push_back("--bind");
    bwrap_argv.push_back(root_path);
    bwrap_argv.push_back("/");

    if (!network) {
        /* Disable network access */
        bwrap_argv.push_back("--unshare-net");
        bwrap_argv.push_back("--unshare-uts");
        bwrap_argv.push_back("--hostname");
        bwrap_argv.push_back("buildbox");
        bwrap_argv.push_back("--unshare-ipc");
    }

    if (command.working_directory().compare("") != 0) {
        bwrap_argv.push_back("--dir");
        bwrap_argv.push_back(command.working_directory());
        bwrap_argv.push_back("--chdir");
        bwrap_argv.push_back(command.working_directory());
    }
    else {
        bwrap_argv.push_back("--chdir");
        bwrap_argv.push_back("/");
    }

    if (d_userNamespaceAvailable) {
        bwrap_argv.push_back("--unshare-user");
        bwrap_argv.push_back("--uid");
        bwrap_argv.push_back(uid);
        bwrap_argv.push_back("--gid");
        bwrap_argv.push_back(gid);
    }

    /* Clear environment */
    for (char **envp = environ; *envp; envp++) {
        const char *assign = strchr(*envp, '=');
        assert(assign);
        const std::string key(*envp, static_cast<size_t>(assign - *envp));

        bwrap_argv.push_back("--unsetenv");
        bwrap_argv.push_back(key);
    }

    /* Set environment for command process */
    for (const auto &env_var : command.environment_variables()) {
        bwrap_argv.push_back("--setenv");
        bwrap_argv.push_back(env_var.name());
        bwrap_argv.push_back(env_var.value());
    }

    /* Give it a proc and tmpfs */
    bwrap_argv.push_back("--proc");
    bwrap_argv.push_back("/proc");
    bwrap_argv.push_back("--tmpfs");
    bwrap_argv.push_back("/tmp");
    bwrap_argv.push_back("--tmpfs");
    bwrap_argv.push_back("/dev/shm");

    static const char *devices[] = {"/dev/full",   "/dev/null", "/dev/urandom",
                                    "/dev/random", "/dev/zero", NULL};

    for (const char **device = devices; *device; device++) {
        bwrap_argv.push_back("--dev-bind");
        bwrap_argv.push_back(*device);
        bwrap_argv.push_back(*device);
    }

    bwrap_argv.insert(bwrap_argv.end(), this->d_bindArguments.cbegin(),
                      this->d_bindArguments.cend());

    /* append command */
    auto &arguments = command.arguments();
    bwrap_argv.insert(bwrap_argv.end(), arguments.begin(), arguments.end());

    return bwrap_argv;
}

bool BubbleWrapRunner::parseArg(const char *arg)
{
    if (arg[0] == '-' && arg[1] == '-') {
        arg += 2;
        const char *assign = strchr(arg, '=');
        if (assign) {
            const std::string key(arg, static_cast<size_t>(assign - arg));
            const char *value = assign + 1;
            if (key == "bind-mount") {
                const char *colon = strchr(value, ':');
                if (!colon) {
                    std::cerr << "Missing `:` in option " << arg << std::endl;
                    return false;
                }

                std::string src(value, static_cast<size_t>(colon - value));
                std::string dest(colon + 1);

                /* Use --dev-bind to allow binding device nodes.
                 * It can be used for regular files and directories as well.
                 */
                this->d_bindArguments.push_back("--dev-bind");
                this->d_bindArguments.push_back(src);
                this->d_bindArguments.push_back(dest);

                return true;
            }
        }
    }

    return false;
}

void BubbleWrapRunner::printSpecialUsage()
{
    std::clog << "    --bind-mount=HOSTPATH:PATH  Bind mount file or "
                 "directory from host into sandbox\n";
}

void BubbleWrapRunner::printSpecialCapabilities()
{
    std::string hostISA = PlatformUtils::getHostISA();

    std::cout << "bind-mount\n";

    std::cout << "platform:OSFamily=" << PlatformUtils::getHostOSFamily()
              << "\n";
    std::cout << "platform:ISA=" << hostISA << "\n";

    if (!d_linux32Path.empty()) {
        // 32-bit compatibility ISAs are supported with `linux32`
        if (hostISA == "x86-64") {
            std::cout << "platform:ISA=x86-32\n";
        }
        else if (hostISA == "aarch64") {
            std::cout << "platform:ISA=aarch32\n";
        }
    }

    if (d_userNamespaceAvailable) {
        std::cout << "platform:unixUID\n";
        std::cout << "platform:unixGID\n";
    }
}

ActionResult BubbleWrapRunner::execute(const Command &command,
                                       const Digest &inputRootDigest)
{
    const auto staged_dir = this->stageDirectory(inputRootDigest);

    std::ostringstream working_directory;
    working_directory << staged_dir->getPath() << "/"
                      << command.working_directory();
    BUILDBOX_LOG_DEBUG("Running in " << working_directory.str());

    ActionResult result;
    {
        createOutputDirectories(command, working_directory.str());
        std::vector<std::string> commandLine =
            generateCommandLine(command, staged_dir->getPath());
        BUILDBOX_LOG_DEBUG("Executing "
                           << logging::printableCommandLine(commandLine))
        executeAndStore(commandLine, &result);
    }

    if (!getSignalStatus()) {
        BUILDBOX_LOG_DEBUG("Capturing command outputs...");
        staged_dir->captureAllOutputs(command, &result);
        BUILDBOX_LOG_DEBUG("Finished capturing command outputs");
    }

    return result;
}

} // namespace bubblewrap
} // namespace buildboxrun
} // namespace buildboxcommon
